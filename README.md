# Creating-User-Interface-on-J2ME-Java-
Creating User Interface on J2ME Java 
import javax.microedition.midlet.*;
import javax.microedition.lcdui.*;

     public class TwoAlerts
	extends MIDlet
        implements CommandListener {
     private Display mDisplay;
	
     private TextBox mTextBox;
     private Alert mTimedAlert;
     private Alert mModalAlert;
	
     private Command mAboutCommand, mGoCommand, mExitCommand;
		
     private Command mAboutCommand, mGoCommand, mExitCommand;
		
     public TwoAlerts(){
         mAboutCommand = new Command("About", Command.SCREEN, 1);
         mGoCommand = new Command("Go", Command.SCREEN, 1);
         mExitCommand = new Command("EXIT", Command.EXIT, 2);

         mTextBox = new TextBox("TwoAlerts", "",32, TextField.ANY);
	 mTextBox.add.Command(mAboutCommand);
	 mTextBox.addCommand(mGoCommand);
	 mTextBox.addCommand(mExitCommand);
	 mTextBox.addSetCommandListener(this);
	     
	mTimedAlert = new Alert("Network error",
	     null,
	     AlertType.INFO);
        mModalAlert = new Alert("About TwoAlerts",
	     "TwoAlerts is a simple MIDlet that demonstrates the use of Alerts. ",
	     null,
	     AlertType.INFO);
	 mModalAlert.setTimeout(Alert.FOREVER);
     }
     
     public void pauseApp(){
     }
     
     public void destroyApp(boolean unconditional) {}
        if(c == mAboutCommand)
          mDisplay.setCurrent(mModalAlert);
       else if (c == mExitCommand)
           notifyDestroyed();
  }       
}	    
